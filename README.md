# restic_backups

## Introduction

This repository defines nomad jobs that invoke restic to backup to S3 the following `$S3_PROJECT`:
* AIMS production cephfs (`AIMS`)
* Koji production cepfs (`KOJI`)
* Daily snapshots created by the snapshotting scripts for Linux releases (Alma,RHEL,Base) `LXSOFT`
* Daily 'yumsnapshot' content `LXSOFT`

The entrypoint `backup.sh` performs a backup of $CANDIDATE using restic, storing the data on `s3-fr-prevessin-1.cern.ch`. Each $CANDIDATE is a separate nomad job.

## Credentials

There are separate credentials used for each "service", 1 pair for `AIMS`, 1 pair for `KOJI` and 1 pair for `LXSOFT` which authenticate to the OpenStack project associated with that service (eg: `AIMS service`, `Koji service`, `Linuxsoft service`. The credentials can be found in the 'Variables' section of <https://gitlab.cern.ch/linuxsupport/cronjobs/restic_backups/-/settings/ci_cd> or alternatively via:

```
tbag show --hg lxsoft/adm restic_backups_AIMS_AWS_ACCESS_KEY_ID
tbag show --hg lxsoft/adm restic_backups_AIMS_AWS_SECRET_ACCESS_KEY

tbag show --hg lxsoft/adm restic_backups_KOJI_AWS_ACCESS_KEY_ID
tbag show --hg lxsoft/adm restic_backups_KOJI_AWS_SECRET_ACCESS_KEY

tbag show --hg lxsoft/adm restic_backups_LXSOFT_AWS_ACCESS_KEY_ID
tbag show --hg lxsoft/adm restic_backups_LXSOFT_AWS_SECRET_ACCESS_KEY
```

The `backup.sh` script exports the variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` which are used by restic, based on the content of `$NOMAD_GROUP_NAME`


## Backups

The docker entry point is the `backup.sh` script. This script will:
* Perform a restic backup with a `tag` of `$TODAY` along with an `exclude_list`. The `exclude_list` is defined as a shell script which populates a file of directories to be excluded. See `base.sh`, `snapshot.sh` or `yumsnapshot.sh` for more details.
* Restic will purge snapshots that are older than `$PRUNE_SNAPSHOTS_OLDER_THAN`
* An email will be sent to administrators detailing the output of the restic job

## Interactive restores

A helper script `interactive.sh` (included in the docker image) provides the ability to query the restic repository in an interactive fashion.

To launch `interactive.sh` it is recommended to run a privileged docker instance as follows:

```bash
    $ docker run -it --rm --pull=always --entrypoint bash \
    --privileged --cap-add SYS_ADMIN --cap-add MKNOD --device /dev/fuse \
    -v /mnt/data1/dist:/data \
    -v /mnt/data2/restic-cache:/cache \
    gitlab-registry.cern.ch/linuxsupport/cronjobs/restic_backups/restic_backups:latest
```

`interactive.sh` provides the following abilities:

1. list snapshots
`./interactive.sh snapshots`
2. restore a snapshot
`./interactive.sh restore YYYYMMDD /path/to/restore/to`
3. fuse mount the restic respository
`./interactive.sh mount` (mounts to `/root/fusemount`)
4. unmount the restic repository
`./interactive.sh unmount`
5. display quota information for the AWS credentials defined
`./interactive.sh quota`

When using `interactive.sh`, the script will ask you to enter S3 credentials. These credentials are stored within the container in `/tmp/credentials`. If you wish to use restic directly (outside of the `interactive.sh` script), you may `source /tmp/credentials` after first running `interactive.sh` (or alternatively `get_credentials.sh`).

Example output from `interactive.sh` to restore the testing snapshot data for AlmaLinux at a point in time (20230508 in this example):

```
[root@6a502de2609a ~]# ./interactive.sh restore 20240612 /data/tmp/restore-20240612
This script defines variables required to access the S3 repo
These variables can be found at https://gitlab.cern.ch/linuxsupport/cronjobs/restic_backups/-/settings/ci_cd
Please define the 'content' to restore
1) AIMS2
2) Koji
3) Yum snapshots
4) Distribution snapshots
Restoration: 4
Which distributions should be the default? (Used for 'snapshot' and 'mount' operations)
For 'restore', the script will ask you again which distribution(s) you wish to restore
1) ALMA8
2) ALMA9
3) RHEL8
4) RHEL9
Distribution: 1
Please enter [distribution] AWS_ACCESS_KEY_ID: <not today>
Please enter [distribution] AWS_SECRET_ACCESS_KEY: <hello there>
Please enter [distribution] RESTIC_PASSWORD: <wow, such secure>
Credentials saved to /tmp/credentials
Credentials found in /tmp/credentials, sourcing them ...
Working with content type: distribution
Which distributions should I restore?
1) Everything (AlmaLinux, RHEL)
2) AlmaLinux
3) RHEL
Distributions: 2
Which releases should I restore?
1) Everything (8,8-testing,9,9-testing)	 3) 9 Only (9,9-testing)		  5) 8 testing				   7) 9 testing
2) 8 Only (8,8-testing)			 4) 8 production			  6) 9 production
Releases: 5
***** Restoring the state of the symlinks for alma / 8-testing as of 20240612 to /data/tmp/restore-20240612 *****
***** Restoring base symlinks to /data/tmp/restore-20240612 *****
repository acf2850f opened successfully, password is correct
found 3 old cache directories in /cache/.restic/cachedir, run `restic cache --cleanup` to remove them
restoring <Snapshot f1e3195f of [/data/cern] at 2024-06-12 13:30:01.395210062 +0200 CEST by root@e53b290dc595> to /data/tmp/restore-20240612
***** Working on alma/8-testing *****
***** Restoring alma/8-testing (snapshot: 8-snapshots/20240612) to /data/tmp/restore-20240612 *****
repository a414f363 opened successfully, password is correct
found 3 old cache directories in /cache/.restic/cachedir, run `restic cache --cleanup` to remove them
restoring <Snapshot a81d8b59 of [/data/cern/alma/8-snapshots] at 2024-06-12 13:30:02.153310037 +0200 CEST by root@825203274b54> to /data/tmp/restore-20240612
```

## Checking quota

Quota can be checked as follows:

```
[root@576b2258513c ~]# ./interactive.sh quota
Credentials found in /tmp/credentials, sourcing them ...
Working with content type: distribution
distribution: 9229 GB / 25600 GB consumed
alma8-backups-production: 874 GB
alma8-backups-testing: 0 GB
alma9-backups-production: 833 GB
alma9-backups-testing: 0 GB
base-backups-production: 164 GB
base-backups-testing: 0 GB
lxsoft-archive: 3872 GB
rhel8-backups-production: 1917 GB
rhel8-backups-testing: 0 GB
rhel9-backups-production: 993 GB
rhel9-backups-testing: 0 GB
yumsnapshot-backups-production: 574 GB
yumsnapshot-backups-testing: 0 GB
yumsnapshots-backups-testing: 0 GB
yumsnapshotsr-backups-testing: 0 GB
[root@576b2258513c ~]#
```

## Initialisation of a new restic store

In the event that we wish to start backing up a new item, there is a manual task required to create the restic repository.
In this example we are defining a new S3 bucket for "rhel10":

```
./get_credentials.sh
source /tmp/credentials
export RESTIC_REPOSITORY="s3:$S3_ENDPOINT/rhel10-backups-production"
restic init
```
