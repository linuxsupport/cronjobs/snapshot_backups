job "${PREFIX}_${JOB}_backups" {
  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  reschedule {
    attempts       = 276
    interval       = "23h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  task "${PREFIX}_${JOB}_backups" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/restic_backups/restic_backups:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_${JOB}_backups"
        }
      }
      mounts = [
        {
          type = "bind"
          target = "/data"
          source = "$DATA"
          readonly = true
          bind_options = {
            propagation = "rshared"
          }
        }
      ]
      volumes = [
        "$CACHE:/cache",
      ]
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      RESTIC_PASSWORD = "$RESTIC_PASSWORD"
      AIMS_AWS_ACCESS_KEY_ID = "$AIMS_AWS_ACCESS_KEY_ID"
      AIMS_AWS_SECRET_ACCESS_KEY = "$AIMS_AWS_SECRET_ACCESS_KEY"
      KOJI_AWS_ACCESS_KEY_ID = "$KOJI_AWS_ACCESS_KEY_ID"
      KOJI_AWS_SECRET_ACCESS_KEY = "$KOJI_AWS_SECRET_ACCESS_KEY"
      LXSOFT_AWS_ACCESS_KEY_ID = "$LXSOFT_AWS_ACCESS_KEY_ID"
      LXSOFT_AWS_SECRET_ACCESS_KEY = "$LXSOFT_AWS_SECRET_ACCESS_KEY"
      S3_PROJECT = "$S3_PROJECT"
      RESTIC_REPOSITORY = "s3:$S3_ENDPOINT/$S3_REPOSITORY"
      PRUNE_SNAPSHOTS_OLDER_THAN = "$PRUNE_SNAPSHOTS_OLDER_THAN"
      SOURCE_PATH = "$SOURCE_PATH"
      EMAIL_FROM = "$EMAIL_FROM"
      EMAIL_ADMIN = "$EMAIL_ADMIN"
      EXCLUDES = "$EXCLUDES"
      TAG = "${PREFIX}_${JOB}_backups"
    }

    resources {
      cpu = 6000 # Mhz
      memory = 12288 # MB
    }

  }
}
