#!/bin/bash
echo "This script defines variables required to access the S3 repo"
echo "These variables can be found at https://gitlab.cern.ch/linuxsupport/cronjobs/restic_backups/-/settings/ci_cd"
echo "Please define the 'content' to restore"
PS3="Content: "
select CONT in "AIMS2" "Koji" "Yum snapshots" "Distribution snapshots"
do
  if [[ $REPLY -eq 1 ]]; then
    CONTENT="aims"
  elif [[ $REPLY -eq 2 ]]; then
    CONTENT="koji"
  elif [[ $REPLY -eq 3 ]]; then
    CONTENT="yumsnapshot"
  elif [[ $REPLY -eq 4 ]]; then
    CONTENT="distribution"
  fi
  break
done
if [ "${CONTENT}" == "distribution" ]; then
  echo "Which distributions should be the default? (Used for 'snapshot' and 'mount' operations)"
  echo "For 'restore', the script will ask you again which distribution(s) you wish to restore"
  PS3="Distribution: "
  select DIST in "ALMA8" "ALMA9" "RHEL8" "RHEL9"
  do
    if [[ $REPLY -eq 1 ]]; then
      DISTRIBUTION="alma8"
    elif [[ $REPLY -eq 2 ]]; then
      DISTRIBUTION="alma9"
    elif [[ $REPLY -eq 3 ]]; then
      DISTRIBUTION="rhel8"
    elif [[ $REPLY -eq 4 ]]; then
      DISTRIBUTION="rhel9"
    fi
    break
  done
fi

echo -n "Please enter [$CONTENT] AWS_ACCESS_KEY_ID: "
read AWS_ACCESS_KEY_ID
echo -n "Please enter [$CONTENT] AWS_SECRET_ACCESS_KEY: "
read AWS_SECRET_ACCESS_KEY
echo -n "Please enter [$CONTENT] RESTIC_PASSWORD: "
read RESTIC_PASSWORD

echo "export CONTENT=$CONTENT" > /tmp/credentials
if [ ! -z $DISTRIBUTION ]; then
  echo "export DISTRIBUTION=$DISTRIBUTION" >> /tmp/credentials
fi
echo "export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID" >> /tmp/credentials
echo "export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY" >> /tmp/credentials
echo "export RESTIC_PASSWORD=$RESTIC_PASSWORD" >> /tmp/credentials
echo "Credentials saved to /tmp/credentials"
